// @flow
import campFeatures from './camp_features'
import CampFeatureInterface from '../components/CampFeature/CampFeatureInterface'

class CampFeaturesServices {
  static getCampFeatures (): Array<CampFeatureInterface> {
    return campFeatures ? campFeatures : []
  }
}

export default CampFeaturesServices