# Hipcamp Challenge

[![Dependency Status](https://david-dm.org/nitrohorse/hipcamp-challenge.svg)](https://david-dm.org/nitrohorse/hipcamp-challenge)
[![License](https://img.shields.io/badge/license-GPLv3-yellow.svg)](https://github.com/nitrohorse/mars-rovers-challenge/blob/master/LICENSE)
[![Standard Style](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/nitrohorse)

[Solution demo](https://hipcamp-nitrohorse.netlify.com/)

## Instructions:
  > Using JavaScript (React preferred), CSS (Sass preferred) and the initial template file campground.html, please reveal the top level camp features as a `<ul>` list, based on the sample JSON structure defined in camp_features.js (let’s assume this JSON structure is what an API could return).
  > Indicate whether or not each feature is present at the campground. Clicking on a feature should reveal its sub-features, if they're present or not, and so on.
  > The presence or absence of each feature may be indicated with an icon, a different color, or both. Feel free to make it look great!
  > Each `<li>` element should be a bubble that's clickable in case there are sub-features to reveal. Sub-features should be displayed in a nested `<ul>` list.
  > Adding or removing features from camp_features.js should automatically update the DOM when the page is reloaded.
  > Clear, concise, "production-ready" code will be appreciated.

## Run Solution Locally
* Clone the repository
* `cd` into the cloned directory
* Install tools:
	* [Node.js and npm](https://www.npmjs.com/get-npm)
	* [Yarn](https://yarnpkg.com/en/)
* Install dependencies: 
	* `yarn` or `npm install`
* Serve:
  * `yarn start` or `npm run start`
* Run tests:
  * `yarn test` or `npm run test`

